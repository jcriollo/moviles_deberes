package statedp

class TipoDeAlerta {
	private var currentState: EstadoDeAlerta? = null

    init {
        currentState = VolumenMinMasVibracion()
    }

    fun setState(state: EstadoDeAlerta) {
        currentState = state
    }

    fun alert() {
        currentState!!.alert(this)
    }
	
}