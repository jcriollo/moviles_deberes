
import builderdp.DespachadorUsuario;
import builderdp.UsarioNormalConstructor;
import builderdp.UsuarioAdministradorConstructor;
import bridgedp.*;
import decorador.*;
import prototypedp.*;
import singledondp.*;
import observerdp.*;
import statedp.*;

fun main (args:Array<String>){
	
	///////////////////////////////////////////////////////////
	/////////////////STATE DESIGN PATERN/////////////////////
	///////////////////////////////////////////////////////////
	  val estato = TipoDeAlerta()
    estato.alert()
    estato.setState(Silencio())
    estato.alert()
    estato.setState(MaxVolumen())
    estato.alert()
	 estato.setState(VolumenMinMasVibracion())
	 estato.alert()
	///////////////////////////////////////////////////////////
	/////////////////OBSEVER DESIGN PATERN/////////////////////
	///////////////////////////////////////////////////////////
	/*val objetoVisible= ObjetoVisible(Modificacion())
	objetoVisible.text= "Inicio"
	objetoVisible.text= "Cambio 1"*/
	

	///////////////////////////////////////////////////////////
	/////////////////SINGLETON DESIGN PATERN/////////////////////
	///////////////////////////////////////////////////////////
	
	/*var s1:Server=Server;
	s1.printName();
	s1.name="servidor1_desarrollo"
	s1.printName();	*/
	
	///////////////////////////////////////////////////////////
	/////////////////PROTOTYPE DESIGN PATTERN/////////////////////
	///////////////////////////////////////////////////////////
	/*fun servicioCoorporativo(servicioBasico: ServicioInternet): ServicioInternet {
    servicioBasico.cambioServicio()
    return servicioBasico
	}
	
	var servicioInternet= ServicioInternet()
	var servicioBasico=servicioInternet.clone()
	val modificacionServicio=servicioCoorporativo(servicioBasico)
	
	println("Servicio de Internet Inicial: Tipo "+servicioInternet.tipodeInternet+"  De  "+servicioInternet.megas+"   Megas"+ "  Precio: "+servicioInternet.precio+"usd")
	println("Servicio de Internet Modificado: Tipo "+servicioBasico.tipodeInternet+"  De  "+servicioBasico.megas+"   Megas"+ "  Precio: "+servicioBasico.precio+"usd")
    println("Servicio de Internet Final: " + modificacionServicio.tipodeInternet+"  De  "+modificacionServicio.megas+"   Megas"+ "  Precio: "+modificacionServicio.precio+"usd")
	*/
	///////////////////////////////////////////////////////////
	/////////////////DECORATOR DESIGN PATTERN/////////////////////
	///////////////////////////////////////////////////////////
	
	/*var compConc=ComponenteConcreto();
	var d1=DecoradorConcretoA("decorador1", compConc);
	var d2=DecoradorConcretoB("decorador2", d1);
	d2.operacion();*/
	
	///////////////////////////////////////////////////////////
	/////////////////BRIDGE DESIGN PATTERN/////////////////////
	///////////////////////////////////////////////////////////
	
	/*var empresaMensajeria=TransLogistic("12345");
	empresaMensajeria.recogerPaquete();
	empresaMensajeria.enviarPaquete();
	empresaMensajeria.entregarPaquete();
	
	
	
	empresaMensajeria.envio=EnvioMar();
	empresaMensajeria.recogerPaquete();
	empresaMensajeria.enviarPaquete();
	empresaMensajeria.entregarPaquete();*/
	
		
	
	///OTHER DESIGN PATTERNS
	/* var creador:Creator;
	creador=CreadorProducto1();
	var producto:Product=creador.factoryMethod();
	producto.operacionGenerica();
	
	
	creador=CreadorProducto2();
	var producto2:Product=creador.factoryMethod();
	producto2.operacionGenerica();*/
	
	/*var creadorfactorydp:FabricaDefactorydp=CreadorUsuarioAdministrador();
	var usuarioAdm=creadorfactorydp.crearUsuario();
	usuarioAdm.definirNivelDeAcceso();
	
	creadorfactorydp=CreadorUsuarioNormal();
	var usuarioNorm=creadorfactorydp.crearUsuario();
	usuarioNorm.definirNivelDeAcceso();
	
	creadorfactorydp=CreadorUsuarioRestringido();
	var usuarioRest=creadorfactorydp.crearUsuario();
	usuarioRest.definirNivelDeAcceso();
	
	
	//uso de clase no generica
	var creadorUsuario=FabricafactorydpNoGen();
	var usrAdmNG=creadorUsuario.obtenerTipoUsurio("adm");
	usrAdmNG.definirNivelDeAcceso();
	
	var usrNormNG=creadorUsuario.obtenerTipoUsurio("nor");
	usrNormNG.definirNivelDeAcceso();
	
	var usrResNG=creadorUsuario.obtenerTipoUsurio("res");
	usrResNG.definirNivelDeAcceso();
	
	var despachadorUsuario:DespachadorUsuario=DespachadorUsuario();
	var usrAdm:UsuarioAdministradorConstructor=UsuarioAdministradorConstructor();
	var usrNrm:UsarioNormalConstructor=UsarioNormalConstructor();
	despachadorUsuario.setConstructorUsuario(usrAdm);
	despachadorUsuario.setConstructorUsuario(usrNrm);*/
}