package bridgedp

abstract class EmpresaMensajeria(var envio:Ienvio) {
	
	public fun recogerPaquete(){
		println("se ha recogido el paqueto ");
		envio.procesarEnvio();
	}
	
	
	public fun enviarPaquete(){
		envio.enviar();
	}
	
	public fun entregarPaquete(){
		envio.procesarEntrega();
		println("se ha entregado el paquete");
	}
	
}