package bridgedp

class EnvioMar:Ienvio{
	override public fun procesarEnvio(){
		println("El paquete se ha cargado en el barco.");
	}
	override public fun enviar(){
		println("El paquete va navegando por el mar.");
	}
	override public fun procesarEntrega(){
		println("El paquete se ha descargado en el puerto.");
	}
}