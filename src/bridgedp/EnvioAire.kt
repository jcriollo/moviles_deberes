package bridgedp

class EnvioAire:Ienvio{
	override public fun procesarEnvio(){
		println("El paquete se ha cargado en el avion.");
	}
	override public fun enviar(){
		println("El paquete va volando por el aire.");
	}
	override public fun procesarEntrega(){
		println("El paquete se ha descargado en el aeropuerto.");
	}
}