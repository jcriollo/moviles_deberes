package builderdp

class UsarioNormalConstructor(usuario:Usuario) : ConstructorUsuario(usuario){
	//public fun constructorUsuarioNormal{super(usuario)}
	public override  fun setNombre(){usuario.setNombre("Pablo Lopez")}
	public override fun setUsername(){usuario.setUsername("plopez")};
	public override fun setPass(){usuario.setPass("p@ssw0rd")};
	public override fun setNivelAcceso(){usuario.setNivelAcceso("usuario normal")};
	
}