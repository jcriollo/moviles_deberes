package builderdp

class UsuarioAdministradorConstructor(usuario:Usuario):ConstructorUsuario(usuario) {
	public override  fun setNombre(){usuario.setNombre("Carlos Lopez")}
	public override fun setUsername(){usuario.setUsername("clopez")};
	public override fun setPass(){usuario.setPass("p@ssw0rd")};
	public override fun setNivelAcceso(){usuario.setNivelAcceso("usuario administrador")};
}