package prototypedp

open class ServicioInternet : Cloneable {
	public var tipodeInternet: String?=null
	public var megas: Int= 0
	public var precio: Double? =0.0
	
	private set
	init {
		tipodeInternet= "Hogar"
		megas= 25
		precio= 26.30
		
	}
	public override fun clone():ServicioInternet{
		return ServicioInternet()
	}
	
	fun cambioServicio(){
		tipodeInternet= "Coorporativo"
		megas= 35
		precio= 56.50
	}
}