package singledondp

object Server{
    var name = "server01"

    init {
        println("Esto solo se ejecutara una unica vez")
    }

    fun printName() = println(name)
}