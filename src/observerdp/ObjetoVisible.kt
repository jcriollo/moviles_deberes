package observerdp
import kotlin.properties.Delegates

class ObjetoVisible (listener: CambioDeValor){
	var text:String by Delegates.observable(
	initialValue="inicia",
		onChange={
			prop,old, new -> listener.onValueChanged(new)
		})
	
}