package observerdp

interface CambioDeValor {
	fun onValueChanged(newValue: String)
}