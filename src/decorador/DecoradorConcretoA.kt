package decorador

class DecoradorConcretoA(private var propiedadAniadida:String,
						  componente:Componente):Decorador(componente) {
	override public fun operacion(){
		super.operacion();
		propiedadAniadida="nueva propiedad";
		println("operacion decoradpr concreto A")
	}
}