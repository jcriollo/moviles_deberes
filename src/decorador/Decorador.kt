package decorador

abstract class Decorador (private var componente:Componente ):Componente(){
	override fun operacion(){
		componente.operacion();
	}
}