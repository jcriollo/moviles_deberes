package decorador

class DecoradorConcretoB(private var propiedadAniadida:String,
						  componente:Componente):Decorador(componente) {
	override public fun operacion(){
		super.operacion();
		comportamientoAniadido();
		propiedadAniadida="nueva propiedad";
		println("operacion decoradpr concreto B")
	}
	
	public fun comportamientoAniadido(){
		println("Se aniade un comportamientpo B");
	}
}